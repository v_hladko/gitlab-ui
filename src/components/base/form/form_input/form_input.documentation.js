import description from './form_input.md';
import examples from './examples/form_input';

export default {
  description,
  examples,
  bootstrapComponent: 'b-form-input',
  followsDesignSystem: true,
};
